package com.epam.testJava;

import com.epam.config.MyConfig;
import com.epam.pages.MainPage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ObjectLock implements Runnable {
    private int number;
    public void run()
    {
        Lock();
    }
    public synchronized void Lock()
    {
        System.out.println(Thread.currentThread().getName());
       // synchronized(ObjectLock.class)
        {
            System.out.println("in block "
                    + Thread.currentThread().getName());
            System.out.println("in block " +
                    Thread.currentThread().getName() + " end");

            number = number +1;
            System.out.println(number);
        }
    }

    public static void main(String[] args) throws InterruptedException, FileNotFoundException {
        ObjectLock g = new ObjectLock();
        Thread t1 = new Thread(g);
        Thread t2 = new Thread(g);
        ObjectLock g1 = new ObjectLock();

        Thread t3 = new Thread(g1);
        t1.setName("t1");
        t2.setName("t2");
        t3.setName("t3");
        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(5000);
        System.out.println("Final Number : "+g.number);

        String filename;
        FileReader fileReader = new FileReader("");
    }

}
