package com.epam.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class Page {

    @Autowired
    TestDriwer testDriwer;

    public Page(){
        System.out.println("Intialized");
    }

    public TestDriwer getTestDriwer() {
        return testDriwer;
    }

    public int getHashCode() {
        return hashCode();
    }
}
