package com.epam.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@EnableAspectJAutoProxy
@Configuration
@ComponentScan("com.epam")
@PropertySource("config.properties")
public class ContextConfig {

    @Autowired
    private Environment env;

    @Bean
    @Lazy
    WebDriver getDriver() {
        switch (env.getProperty("browser")) {
            case "Firefox":
                return new FirefoxDriver();
            case "Chrome":
                System.setProperty("webdriver.chrome.driver", env.getProperty("driverPath"));
                return new ChromeDriver();
            default:
                //System.setProperty("","");
                return new ChromeDriver();
        }
    }


}

