package com.epam;

import com.epam.page.HomePage;
import com.epam.page.LoginPage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {


    public static void main(String... arg) {
       ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);

        LoginPage loginPage = applicationContext.getBean( LoginPage.class);
        loginPage.printmessage();

        HomePage homePage = applicationContext.getBean(HomePage.class);
        homePage.printmessage();

        //Employee employee2 = applicationContext.getBean( Employee.class);
        //System.out.println(employee);
        //System.out.println(employee.getName());
        ///System.out.println(employee);
       // LoginPage loginPage = applicationContext.getBean("loginPage", LoginPage.class);
        //System.out.println(loginPage.getHashCode() +" : "+loginPage.getName());

        //HomePage homePage = applicationContext.getBean("homePage",HomePage.class);
        //System.out.println(homePage.getHashCode());
        /*

    */}
}
