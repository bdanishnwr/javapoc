package com.epam.aop;

import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

public class Logger {

    @Before("pageAction()")
    public void log() {
        System.out.println("Logger Class : - " + Thread.currentThread().getId());
        System.out.println("Logging");
        try {

            Thread.sleep(1000);
        } catch (Exception a) {

        }
    }

    @Pointcut("execution(* com.epam.Employee.*(..))")
    public void pageAction() {
    }

}
