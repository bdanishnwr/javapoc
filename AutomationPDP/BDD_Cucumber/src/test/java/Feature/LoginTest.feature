Feature: Login Test

  Scenario: Test Login feature with correct username and password
    Given User Navigated to home page
    And Enter email address danish
    And Enter my salary 10000
    And User enter username as
      | UserName | Password |
      | Danish   | Anwer    |
    And User clicked Login Button
    Then User should navigate to dashboard

  Scenario Outline: Test Outline Login feature with correct username and password
    Given User Navigated to home page
    And User enter username <UserName> and <Password>
    And User clicked Login Button
    Then User should navigate to dashboard
    Examples:
      | UserName  | Password|
      | qa  | test |