package com.epam;

public class DataConnection {
    private int testData;


    public void setTestData(int testData) {
        this.testData = testData;
    }

    public int getTestData() {
        return testData;
    }

    @Override
    public String toString() {
        return "Test Data : "+testData;
    }
}
