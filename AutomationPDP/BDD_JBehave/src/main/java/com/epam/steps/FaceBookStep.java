package com.epam.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class FaceBookStep {

    @Given("Open Facebook in Browser")
    public void givenOpenFacebookInBrowser() {
        System.out.println("Logging: Open Facebook in Browser");
    }

    @When("I Login with credential Danish")
    public void whenILoginWithCredentialDanish() {
        System.out.println("Logging: I Login with credential Danis");
    }

    @Then("Logged in succefully")
    public void thenLoggedInSuccefully() {
        System.out.println("Logging: Open Facebook in Browser");
    }
}
