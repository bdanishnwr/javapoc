package com.epam;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class Base {

    @BeforeTest
    public void beforeTest(){
        baseURI = "https://reqres.in/";
    }

    @Test
    public void testLogin(){
        get("/api/users/2").then()
                .body("data.id",equalTo(2))
                .body("data.first_name",is("Janet"))
                ;

        get("/add").as(Base.class);

    }
}
