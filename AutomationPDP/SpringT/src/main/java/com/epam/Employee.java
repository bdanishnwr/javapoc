package com.epam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;


public class Employee {
    @Value("${emp.Name}")
    private String name;
    private Integer age;

    @Autowired
    @Qualifier("second")
    private Address address;

    @Autowired
    @Qualifier("second")
    private Address address2;

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", age=" + age +
                '}';
    }

    public Employee(String name, Integer age) {
        System.out.println("Intialized");
       // this.name = name;
        this.age = age;
    }

    public Employee(){

    }


    public String getName() {
        System.out.println("Bean Class :- " + Thread.currentThread().getId());
        return name;
    }

}
