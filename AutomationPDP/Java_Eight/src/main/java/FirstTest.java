public class FirstTest {

    public static int[] sortElement(int[] givenList) {
        for (int x = 0; x < givenList.length - 1;) {
            if (givenList[x] > givenList[x + 1]) {
                givenList[x] += givenList[x + 1];
                givenList[x + 1] = givenList[x] - givenList[x + 1];
                givenList[x] -= givenList[x + 1];
                x = 0;
            } else
                x++;
        }
        return givenList;
    }

    /**
     * Write a java program to remove duplicate elements from an integer array
     * without using java collections.
     */
    public static int[] removeDuplicateInteger(int givenList[]) {
        givenList = sortElement(givenList);
        int tempList[] = new int[givenList.length];
        for (int j = 0, i = 0; i < givenList.length - 1; i++) {
            if (givenList[i] != givenList[i + 1]) {
                tempList[j++] = givenList[i + 1];
            }
        }
        return tempList;
    }

    /**
     * Write a java program to separate integers and chars from a given string.
     */
    public static String seprateIntChar(String givenString) {
        String charString = "", numString = "";
        for (int x = 0; x < givenString.length(); x++) {
            if (givenString.charAt(x) >= '0' && givenString.charAt(x) <= '9')
                numString += givenString.charAt(x);
            else
                charString += givenString.charAt(x);

        }
        return charString + " " + numString;
    }

    /**
     * write a java program to find first non-repeated char in a given string
     * without using collections - example "hello world" - h is first non
     * -repeating char
     */
    public static char findOutFirstRepeatedChar(String givenString) {
        givenString = givenString.toLowerCase();
        for (int x = 0; x < givenString.length(); x++)
            if (!givenString.replaceFirst(givenString.charAt(x) + "", "")
                    .contains(givenString.charAt(x) + ""))
                return givenString.charAt(x);
        return '0';
    }

    /**
     * write a java program to 2nd largest number in a given integer array.
     */
    public static int findSecondLargestNumber(int givenNumList[]) {
        givenNumList = removeDuplicateInteger(givenNumList);
        for (int i = 1; i < givenNumList.length; i++)
            if (givenNumList[i] == 0)
                return givenNumList[i - 2];
        return 0;
    }

    /**
     * write java program to check if two strings are anagrams
     */
    public static boolean isAnagrams(String firstString, String secondString) {
        for (int x = 0; x < firstString.length(); x++) {
            firstString = firstString.replaceFirst(secondString.charAt(x) + "", "");
            if (firstString.length() != secondString.length() - 1 -x ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Write a java program to print elements from a string array which has all
     * the chars in a given char array.
     */

    public static void printElementWithAllChar(String stringList[],
                                               char[] charList) {
        boolean flag = true;
        for (int x = 0; x < stringList.length; x++) {
            for (int y = 0; y < charList.length; y++) {
                flag = stringList[x].contains(charList[y] + "")
                        || stringList[x].toLowerCase().contains(
                        charList[y] + "");
                if (!flag)
                    break;
            }
            if (flag)
                System.out.println(stringList[x]);
        }

    }

    /**
     * Write a program to reverse a number using numeric operations.
     */

    public static int reverseNumber(int num) {
        int rev = 0, rem = 0;
        while (num > 0) {
            rem = num % 10;
            num = num / 10;
            rev = rem + rev * 10;
        }
        return rev;
    }

    public static boolean isPrime(int num) {
        for (int x = 2; x < num; x++) {
            if (num % x == 0)
                return false;
        }
        return true;
    }

    /**
     * Write a program to find the sum of the first 1000 prime numbers.
     */
    public static int sumOfPrimeNumber(int number) {
        int x = 0, sum = 0;
        for (int count = 0; count < number; x++) {
            if (isPrime(x)) {
                sum += x;
                count++;
            }
        }
        return sum;
    }

    public static void main(String[] args) {

        /*int x[] = { 0, 8, 9, 5, 10, 5, 6, 4, 7, 4, 10, 11 };

        // Removing Duplicate
        x = removeDuplicateInteger(x);
        for (int i = 0; i < x.length; i++)
            System.out.println(x[i]);

        // Separating Int char
        System.out.println(seprateIntChar("10259Dani02s5H"));

        System.out.println(findOutFirstRepeatedChar("helloWeorldw"));

        System.out.println(findSecondLargestNumber(x));

        System.out.println(isAnagrams("Angel", "gelAn"));

        String listWord[] = { "", "acd", "Danish" };
        char[] charList = { 'a', 'c', 'd' };
        printElementWithAllChar(listWord, charList);

        System.out.println(reverseNumber(567));
        System.out.println(sumOfPrimeNumber(1000));*/


        System.out.println(isAnagrams("ddsd","dsdd"));
    }

}