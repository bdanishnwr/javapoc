package com.epam.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyConfig extends AnnotationConfigApplicationContext {

    private static MyConfig myConfig;

    private MyConfig() {
        super(ContextConfig.class);
    }

    public static MyConfig getInstance() {
        if (myConfig == null) {
            myConfig = new MyConfig();
        }
        return myConfig;
    }

}
