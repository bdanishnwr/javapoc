package com.epam;

public class Address {

    private String cityName;
    private String state;
    private Integer pinCode;

    public Address(String cityName, String state, int pinCode) {
        this.cityName = cityName;
        this.state = state;
        this.pinCode = pinCode;
        System.out.println("Test :Intiailized");
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "cityName='" + cityName + '\'' +
                ", state='" + state + '\'' +
                ", pinCode=" + pinCode +
                '}';
    }
}
