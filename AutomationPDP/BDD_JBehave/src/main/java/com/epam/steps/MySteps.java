package com.epam.steps;

import com.epam.config.MyConfig;
import com.epam.pages.HomePage;
import org.apache.commons.io.FileUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class MySteps {

    @Given("Open $app in Browser")
    public void openApp(String app) {
        WebDriver driver = MyConfig.getInstance().getBean(WebDriver.class);
        driver.get(app);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver,5);

        //wait.until(ExpectedConditions.)


    }

    @When("I Search $name")
    public void searchTest(String name) {
        HomePage homePage = MyConfig.getInstance().getBean(HomePage.class);
        homePage.searchString(name);

    }

    @Then("I Get Result $name")
    public void getResult(String name) {
        WebDriver driver = MyConfig.getInstance().getBean(WebDriver.class);
        WebElement element = driver.findElement(By.xpath("//*[contains(text(),'" + name + "')]"));
        if (element != null) {
            System.out.println("Ok");

            File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(screenshotFile, new File("D:/screenshot.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else System.out.println("Not Found");
    }

}
