package steps;

import Base.BaseUtil;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hook extends BaseUtil {

    private BaseUtil base;

    public Hook(BaseUtil base) {
        System.out.println("Executed Hook Constructor \n\n");
        this.base = base;
    }

    @Before
    public void beforeScenario() {
        System.out.println("Before Start");
        base.stepInfo = "Webdriver FireFox";
    }

    @After
    public void afterScenario() {
        System.out.println("After Scenario");
    }
}
