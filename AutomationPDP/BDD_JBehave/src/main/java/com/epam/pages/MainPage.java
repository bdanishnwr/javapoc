package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class MainPage {

    @FindBy(xpath = "//input[@id=\"fromCity\"]")
    WebElement fromLink;

    @FindBy(xpath = "//input[@id=\"toCity\"]")
    WebElement toLink;

    @FindBy(xpath = "//input[@placeholder=\"From\"]")
    WebElement fromSource;

    @FindBy(xpath = "//input[@placeholder=\"To\"]")
    WebElement toDestination;

    @FindBy(xpath = "//input[@id=\"departure\"]")
    WebElement departureDate;

    @FindBy(xpath = "//a[contains(text(),\"Search\")]")
    WebElement searchBtn;

    WebDriver driver;

    public MainPage(@Autowired WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void setFromSource(String fromSourceName) {
        fromLink.click();
        fromSource.sendKeys(fromSourceName);

    }


}
