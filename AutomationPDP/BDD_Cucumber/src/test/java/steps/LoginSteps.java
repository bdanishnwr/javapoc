package steps;

import Base.BaseUtil;
import TransformT.SalaryTransform;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.List;

public class LoginSteps extends BaseUtil {

    private BaseUtil base;

    public LoginSteps(BaseUtil base) {
        System.out.println("Executed LoginStep Constructor \n\n");


        this.base = base;
    }

    @Given("^User Navigated to home page$")
    public void userNavigatedToHomePage() {
        System.out.println("Driver Got :" + base.stepInfo + "\n");
    }

    @And("^User clicked Login Button$")
    public void userClickedLoginButton() {

    }

    @Then("^User should navigate to dashboard$")
    public void userShouldNavigateToDashboard() {
    }


    @And("^User enter username as$")
    public void userEnterUsernameAs(DataTable table) {
        // List<List<String>> data = table.raw();
        // System.out.println("UserName :" + data.get(1).get(0));

        List<User> userData = table.asList(User.class);

        for (User userD : userData) {
            System.out.println(userD.userName);
            System.out.println(userD.password);
        }

    }

    @And("^User enter username ([^\"]*) and ([^\"]*)$")
    public void userEnterUsernameUserNameAndPassword(String userName, String password) {
        System.out.println("UserName :" + userName + " , Password : " + password);
    }

    @And("^Enter email address ([^\"]*)$")
    public void enterEmailAddress(@Transform(TransformT.EmailTransform.class) String emailAddress) {
        System.out.println("Email Address : " + emailAddress + " \n\n");
    }

    @And("^Enter my salary (\\d+)$")
    public void enterMySalary(@Transform(SalaryTransform.class) int arg0) {
        System.out.println("My Slary digit is :" + arg0);
    }


    public class User {
        public String userName;
        public String password;

        public User(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }
    }
}
