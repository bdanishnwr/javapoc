package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PopUpHandler {
    WebDriver webDriver;

    boolean flag = true;
    @FindBy(xpath = "popupXpath")
    private WebElement popupIdentifier;

    public PopUpHandler(@Autowired WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
        startPopUpHandler();

    }


    private synchronized void startPopUpHandler() {

        Thread popUpHandler = new Thread(() -> {
            while (flag) {
                if (flag && isPopUpAppeared()) {
                    System.out.println("Pop Up Handling");
                } else
                    System.out.println("Pop Not Appeared");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        popUpHandler.setDaemon(true);
        popUpHandler.start();

    }

    private boolean isPopUpAppeared() {
        try {
            if (flag)
                return popupIdentifier.isDisplayed();
        } catch (Exception a) {
        }
        return false;
    }

    public void stop() {
        flag = false;
    }
}
