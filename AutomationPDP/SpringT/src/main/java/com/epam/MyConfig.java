package com.epam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan({"com.epam","com.epam.aop"})
@PropertySource("employee.properties")
@EnableAspectJAutoProxy
public class MyConfig {



    @Bean
    @Lazy
    Employee emplpoyee(@Autowired  Environment environment) {
        return new Employee("Ok", environment.getProperty("emp2.Age" , Integer.class));
    }


    @Bean("first")
    @Lazy
    Address myAddress() {
        return new Address("Hyd", "Tel", 5008);
    }

    @Bean("second")
    @Lazy
    Address mySecondAddress() {
        return new Address("Ranchi", "Jharkhand", 5008);
    }

}