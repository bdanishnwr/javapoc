package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class HomePage {

    WebDriver webDriver;

    @FindBy(xpath = "/home")
    private WebElement homePage;

    @FindBy(xpath = "//input[@title='Search']")
    private WebElement searchField;

    @FindBy(xpath = "//input[@type='submit' and @value='Google Search']")
    private WebElement submitSearch;


    public HomePage(@Autowired WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public void searchString(String word){
        searchField.sendKeys(word);
        submitSearch.click();
    }

}
