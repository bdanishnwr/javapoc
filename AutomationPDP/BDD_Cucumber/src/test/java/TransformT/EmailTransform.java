package TransformT;

import cucumber.api.Transformer;

public class EmailTransform extends Transformer<String> {
    public String transform(String s) {
        return s.concat("@gmail.com");
    }
}
