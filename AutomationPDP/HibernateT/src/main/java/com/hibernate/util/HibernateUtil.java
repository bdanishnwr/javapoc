package com.hibernate.util;

import org.hibernate.SessionFactory;

public class HibernateUtil {
    private static HibernateUtil hibernateUtilInstance ;
    private static SessionFactory sessionFactory;
    private HibernateUtil(){}
    public static HibernateUtil getHibernateUtilInstance(){
        if(hibernateUtilInstance==null)
            hibernateUtilInstance = new HibernateUtil();
        return hibernateUtilInstance;
    }
}
