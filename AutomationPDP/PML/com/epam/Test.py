class Person:
    def __init__(self):
        self.data = 10


class Main:
    def __init__(self):
        self.role = 30
        return


class Test(Person,Main):
    def test(self):
        return "Hello"


class A:
    def __init__(self):
        self.name = 'John'
        self.age = 23

    def getName(self):
        return self.name+"Hello"


class B:
    def __init__(self):
        self.name = 'Richard'
        self.id = '32'

    def getName(self):
        print("B")
        return self.name


class C(B, A):
    def __init__(self):
        B.__init__(self)
        A.__init__(self)


def double_input():
    while True:
        print("Running")
        x = yield
        print(x)
        yield x * 2


gen = double_input()
next(gen)
gen.send(7)
next(gen)
next(gen)

