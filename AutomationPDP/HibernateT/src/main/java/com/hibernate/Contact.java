package com.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "contact")
public class Contact {
    private  int id;
    private  String name;
    private  String email;

    public Contact(){

    }
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Contact(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    @Id
    public int getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }
}
