package com.epam.steps;

import com.epam.config.MyConfig;
import com.epam.pages.PopUpHandler;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.WebDriver;

public class Setup {
    @BeforeStory
    public void beforeStory() {

    }

    @AfterStory
    public void afterStory(){
        PopUpHandler popUpHandler = MyConfig.getInstance().getBean(PopUpHandler.class);
        popUpHandler.stop();
        WebDriver driver = MyConfig.getInstance().getBean(WebDriver.class);
        driver.close();
    }
}
